#include<stdio.h>
int input_size();
void input(int *,int);
void cmp(int ,int *,int *,int *,int *,int *);
void output(int ,int *,int,int,int,int);
int main()
{
    int n,s,l,is,il;
    n=input_size();
    int a[n];
    input(a,n);
    cmp(n,a,&s,&l,&is,&il);
    output(n,a,s,l,is,il);
    return 0;
}
int input_size()
{
    int n;
    printf("enter the size of array\n");
    scanf("%d",&n);
    return n;
}

void input(int a[],int n)
{
    int i;
    printf("enter %d elements\n",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}
void cmp(int n,int a[],int *s,int *l,int *is,int *il)
{
    int i;
    *s=a[0];
    *l=a[0];
    for(i=0;i<n;i++)
    {
        if(*s>a[i])
        {
            *s=a[i];
            *is=i;
        }
        if(*l<a[i])
        {
            *l=a[i];
            *il=i;
        }
    }
    a[*is]=*l;
    a[*il]=*s;
}
void output(int n,int a[],int s,int l,int is,int il)
{
    printf("the largest element is %d in %d index \n",l,il);
    printf("the smallest element is %d in %d index \n",s,is);
    for(int i=0;i<n;i++)
    {
        printf("%d   ",a[i]);
    }
}