#include <stdio.h>
int main()
{
    struct library
    {
        char bk_name[20];
        char due_date[7];
        char date_issue[7];
    };
    struct student
    {
        int reg_no;
        char name[10];
        char dept[10];
        struct library book;
    };
    struct student s;
    printf("enter student details\n");
    printf("reg number  ");
    scanf("%d",&s.reg_no); 
    printf("name  ");
    scanf("%s",s.name);
    printf("department  ");
    scanf("%s",s.dept);
    printf("enter book details\n");
    printf("book name  ");
    scanf("%s",s.book.bk_name);
    printf("date of issue (ddmmmyy) ");
    scanf(" %s",s.book.date_issue);
    printf("due date ");
    scanf("%s",s.book.due_date);
    printf("\n");
    printf("ROLL No. ->  %d",s.reg_no);
    printf("NAME  -> %s\n",s.name);
    printf("DEPARTMENT ->  %s\n",s.dept);
    printf("TITLE OF THE BOOK BORROWED -> %s",s.book.bk_name);
    printf("date of issue -> %s\n",s.book.date_issue);
    printf("due date -> %s",s.book.due_date);
    return 0;
}