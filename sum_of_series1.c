// series is 1/1! + 2/2! + 3/3! + 4/4! + . . . . . . 
#include<stdio.h>
int fact(int);
int main()
{
    int n,i;
    float x,sum=0;
    printf("how many terms \n");
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        x=fact(i);
        sum=sum+(float)i/x;
    }
    printf("the sum of the terms is %f ",sum);
    return 0;
}
int fact(int i)
{
    int j,f=1;
    for(j=1;j<=i;j++)
    {
        f=f*j;
    }
    return f;
}