#include<stdio.h>
int main()
{
    int n,e,i,l,h,m,f=0;
    printf("enter the size of the array\n");
    scanf("%d",&n);
    int a[n];
    printf("enter %d elements of array in ascending order\n",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("enter the element to be found\n");
    scanf("%d",&e);
    l=0;
    h=n-1;
    while(l<=h)
    {
        m=(l+h)/2;
        if(a[m]==e)
        {
            printf("the element is found in position %d",m+1);
            f=1;
            break;
        }
        else if(a[m]<e)
            l=m+1;
        else
        h=m-1;
    }
    if(f==0)
    {
        printf("the element was not  found in the array\n");
    }
    return 0;
}
